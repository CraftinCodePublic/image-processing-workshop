## Materiał stworzony przez szkołę programowania [Craftin' Code](https://craftincode.com)

Warsztaty są wprowdzeniem do świata wielowątkowości w Javie oraz przetwarzania obrazów. Poruszone zostaną podstawowe elementy wielowątkowości (`Thread,Runnable,Executor,Callable,Future`). Jeśli chcesz dokładnie poznać wielowątkowość oraz zaganienia takie jak **Git, Maven, typy generyczne, SQL i bazy danych, wzorce projektowe, JSON, REST, JUnit** zapraszamy na nasz [Ekspresowy Kurs Java - poziom średnio zaawansowany](https://www.craftincode.com/ekspresowy-kurs-java-srednio-zaawansowany-wroclaw/).

Zapraszamy również do polubienia naszego [fanpage'a na Facebooku](https://www.facebook.com/craftincode/) gdzie na bieżąco publikujemy informacje na temat naszych kursów, wydarzeń oraz przydatne informacje wszystkim, którzy uczą się programowania.

## Pobranie projektu
Projekt jest oparty o narzędzie `Maven`, które automatycznie konfiguruje projekt oraz ściąga potrzebne biblioteki. Projekt można bezpośrednio pobrać i otworzyć korzystając z `IntelliJ IDEA` - `VCS -> Checkout from version control -> Git` a następnie podając jako URL adres powyższego repozytorium - **git@gitlab.com:CraftinCodePublic/image-processing-workshop.git**