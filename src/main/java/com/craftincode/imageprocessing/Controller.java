package com.craftincode.imageprocessing;

import com.craftincode.imageprocessing.processing.FilterTasksRunner;
import com.craftincode.imageprocessing.processing.FilterType;
import com.craftincode.imageprocessing.processing.FilteringResult;
import javafx.application.Platform;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.Initializable;
import javafx.scene.control.*;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.image.WritableImage;
import javafx.scene.layout.GridPane;
import javafx.stage.FileChooser;

import java.io.File;
import java.io.InputStream;
import java.net.URL;
import java.util.ResourceBundle;

public class Controller implements Initializable {
    public static final String[] PREDEFINED_IMAGES = {"image1-girl.jpg", "image2-map.jpg", "image3-map.jpg"};
    public ImageView imageView;
    public ProgressBar progressBar;
    public GridPane pane;
    public Label infoLabel;
    public ComboBox<Integer> threadsNumberComboBox;
    public ListView<String> logList;
    public SplitMenuButton loadPredefinedImage;
    public ComboBox<FilterType> filterTypeComboBox;
    public Button loadImageFileButton;
    public Button saveImageButton;
    public SplitMenuButton processSplitButton;
    private WritableImage image;
    private Image originalImage;
    private ControllerUtils controllerUtils = new ControllerUtils();

    public void loadImageFile(ActionEvent actionEvent) {
        FileChooser fileChooser = createJpegFileChooser("Load image from file");
        File file = fileChooser.showOpenDialog(null);
        if (file != null) {
            loadImageFromFile(file);
        }
    }

    private FileChooser createJpegFileChooser(String s) {
        FileChooser fileChooser = new FileChooser();
        fileChooser.setTitle(s);

        FileChooser.ExtensionFilter extFilter =
                new FileChooser.ExtensionFilter("JPEG Files", "*.jpeg");
        fileChooser.getExtensionFilters().add(extFilter);
        return fileChooser;
    }

    private void loadImageFromFile(File file) {
        setProgressIndeterminate();
        setButtonsDisabled(true);

        new Thread(() -> {
            originalImage = new Image("file:" + file.getAbsolutePath());

            try {
                Thread.sleep(3000);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            resetToOriginalImage();
            log("Loaded file: " + file.getAbsolutePath() + " (" + (int) image.getWidth() + "x" + (int) image.getHeight() + ")");
            setProgressComplete();
            setButtonsDisabled(false);
        }).start();
    }

    private void setProgressComplete() {
        Platform.runLater(() -> progressBar.setProgress(1));
    }

    private void setProgressIndeterminate() {
        Platform.runLater(() ->
                progressBar.setProgress(ProgressBar.INDETERMINATE_PROGRESS));
    }

    private void loadImageFromResources(String name) {
        setButtonsDisabled(true);
        setProgressIndeterminate();

        new Thread(() -> {
            InputStream resourceAsStream = getClass().getClassLoader().getResourceAsStream(name);
            originalImage = new Image(resourceAsStream);

            resetToOriginalImage();
            log("Loaded predefined file: " + name + " (" + (int) image.getWidth() + "x" + (int) image.getHeight() + ")");
            setProgressComplete();
            setButtonsDisabled(false);
        }).start();
    }

    private void log(String text) {
        Platform.runLater(() ->
                logList.getItems().add(text)
        );
    }

    private void resetToOriginalImage() {
        image = controllerUtils.convertImageToWritableImage(originalImage);
        imageView.setImage(originalImage);
    }

    public void process(ActionEvent actionEvent) throws InterruptedException {

        processMultipleTimes(1);
    }

    private void setButtonsDisabled(boolean disabled) {
        Platform.runLater(() -> {
            loadImageFileButton.setDisable(disabled);
            saveImageButton.setDisable(disabled);
            filterTypeComboBox.setDisable(disabled);
            threadsNumberComboBox.setDisable(disabled);
            loadPredefinedImage.setDisable(disabled);
            processSplitButton.setDisable(disabled);
        });
    }

    private void setImage(WritableImage resultImage) {
        imageView.setImage(resultImage);
        this.image = resultImage;
    }

    @Override
    public void initialize(URL location, ResourceBundle resources) {
        ObservableList<Integer> options =
                FXCollections.observableArrayList(
                        1, 2, 4, 8, 20, 100, 1000, 10000, 50000
                );
        threadsNumberComboBox.setItems(options);
        threadsNumberComboBox.setValue(1);


        for (String predefinedImage : PREDEFINED_IMAGES) {
            MenuItem menuItem = new MenuItem(predefinedImage);
            menuItem.setOnAction(event -> {
                loadImageFromResources(predefinedImage);
            });
            loadPredefinedImage.getItems().add(menuItem);
        }

        for (FilterType filterType : FilterType.values()) {
            filterTypeComboBox.getItems().add(filterType);
        }
        filterTypeComboBox.setValue(filterTypeComboBox.getItems().get(0));

        processSplitButton.getItems().add(createProcessMultipleTimesMenuItem("Process 5x", 5));
        processSplitButton.getItems().add(createProcessMultipleTimesMenuItem("Process 10x", 10));

        setButtonsDisabled(true);
        Platform.runLater(() -> {
            loadPredefinedImage.setDisable(false);
            loadImageFileButton.setDisable(false);
        });
    }

    private MenuItem createProcessMultipleTimesMenuItem(String text, int times) {
        MenuItem menuItem1 = new MenuItem(text);
        menuItem1.setOnAction(event -> processMultipleTimes(times));
        return menuItem1;
    }

    private void processMultipleTimes(int times) {
        setButtonsDisabled(true);
        int threadsNumber = threadsNumberComboBox.getValue();
        FilterType filterType = filterTypeComboBox.getValue();

        FilterTasksRunner filterTasksRunner = new FilterTasksRunner(threadsNumber, image, filterType, times);

        filterTasksRunner.setOnSucceeded(event -> {
            FilteringResult filteringResult = filterTasksRunner.getValue();

            log(times + "x " + filteringResult.getFilterName() + " ended in " + filteringResult.getDurationInSeconds() + "s, " + filteringResult.getThreadsNumber() + " threads");
            setImage(filteringResult.getImage());
            setButtonsDisabled(false);
            progressBar.progressProperty().unbind();
        });
        progressBar.progressProperty().bind(filterTasksRunner.progressProperty());

        new Thread(filterTasksRunner).start();
    }

    public void saveImageToFile(ActionEvent actionEvent) {
        FileChooser fileChooser = createJpegFileChooser("Save image to file");
        File file = fileChooser.showSaveDialog(null);
        if (file != null) {
            controllerUtils.saveImageToFile(file, imageView);
        }
    }
}
