package com.craftincode.imageprocessing.processing.filters;

import com.craftincode.imageprocessing.processing.FilterTask;
import javafx.scene.image.WritableImage;
import javafx.scene.paint.Color;

public class InvertFilter extends FilterTask {
    public InvertFilter(WritableImage fromImage, WritableImage toImage, int startX, int endX) {
        super(fromImage, toImage, startX, endX);
    }

    @Override
    protected void process() {
        for (int x = startX; x < endX; x++) {
            for (int y = 0; y < height; y++) {
                Color oldPixel = pixelReader.getColor(x, y);

                double newR = 1 - oldPixel.getRed();
                double newG = 1 - oldPixel.getGreen();
                double newB = 1 - oldPixel.getBlue();

                Color newPixel = new Color(newR, newG, newB, 1);

                pixelWriter.setColor(x, y, newPixel);
            }
        }
    }
}
