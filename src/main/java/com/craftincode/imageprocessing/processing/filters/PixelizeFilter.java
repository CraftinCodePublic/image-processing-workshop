package com.craftincode.imageprocessing.processing.filters;

import com.craftincode.imageprocessing.processing.FilterTask;
import javafx.scene.image.WritableImage;
import javafx.scene.paint.Color;

public class PixelizeFilter extends FilterTask {
    private int tileSize; // pixel size (x and y)

    public PixelizeFilter(WritableImage fromImage, WritableImage toImage, int startX, int endX, int tileSize) {
        super(fromImage, toImage, startX, endX);
        this.tileSize = tileSize;
    }

    @Override
    protected void process() {
        for (int x = startX; x < endX; x += 100) {
            for (int y = 0; y < height; y += 100) {

                Color average = getAverageColor(x, y);

                setColorInRange(average, x, y);
            }
        }
    }

    private void setColorInRange(Color average, int pixelX, int pixelY) {
        for (int x = pixelX; x < pixelX + 100 && x < endX; x++) {
            for (int y = pixelY; y < pixelY + 100 && y < height; y++) {
                pixelWriter.setColor(x, y, average);
            }
        }
    }

    private Color getAverageColor(int pixelX, int pixelY) {
        double rSum = 0;
        double gSum = 0;
        double bSum = 0;

        for (int x = pixelX; x < pixelX + 100 && x < endX; x++) {
            for (int y = pixelY; y < pixelY + 100 && y < height; y++) {
                Color pixel = pixelReader.getColor(x, y);
                rSum += pixel.getRed();
                gSum += pixel.getGreen();
                bSum += pixel.getBlue();
            }
        }

        double avgR = rSum / (100 * 100);
        double avgG = gSum / (100 * 100);
        double avgB = bSum / (100 * 100);

        return new Color(avgR, avgG, avgB, 1);
    }
}
