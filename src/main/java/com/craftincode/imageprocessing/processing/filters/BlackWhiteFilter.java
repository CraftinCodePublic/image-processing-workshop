package com.craftincode.imageprocessing.processing.filters;

import com.craftincode.imageprocessing.processing.FilterTask;
import javafx.scene.image.WritableImage;
import javafx.scene.paint.Color;

public class BlackWhiteFilter extends FilterTask {
    public BlackWhiteFilter(WritableImage fromImage, WritableImage toImage, int startX, int endX) {
        super(fromImage, toImage, startX, endX);
    }

    @Override
    protected void process() {
        for (int x = startX; x < endX; x++) {
            for (int y = 0; y < height; y++) {
                Color oldPixel = pixelReader.getColor(x, y);

                double avg = (oldPixel.getRed() + oldPixel.getGreen() + oldPixel.getBlue())/3;

                Color newPixel = new Color(avg, avg, avg, 1);

                pixelWriter.setColor(x, y, newPixel);
            }
        }
    }
}
