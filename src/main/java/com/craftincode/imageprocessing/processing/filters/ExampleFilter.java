package com.craftincode.imageprocessing.processing.filters;

import com.craftincode.imageprocessing.processing.FilterTask;
import javafx.scene.image.WritableImage;
import javafx.scene.paint.Color;

public class ExampleFilter extends FilterTask {
    public ExampleFilter(WritableImage fromImage, WritableImage toImage, int startX, int endX) {
        super(fromImage, toImage, startX, endX);
    }

    @Override
    protected void process() {
        /*
            TODO
            startX, endX - X coordinates (from-to)
            width, height - image dimensions
            Color pixel = pixelReader.getColor(x,y); - fetching pixel from source image
            Color newPixel = new Color(red,green,blue,1); - creating new pixel; red,green,blue - RGB value (0.0-1.0)
            pixelWriter.setColor(x,y,newPixel); - set pixel in destination image
         */

        for (int x = startX; x < endX; x++) {
            for (int y = 0; y < height; y++) {
                Color color = pixelReader.getColor(x, y);
                pixelWriter.setColor(x, y, color);
            }
        }
    }
}
