package com.craftincode.imageprocessing.processing;

import com.craftincode.imageprocessing.processing.filters.BlackWhiteFilter;
import com.craftincode.imageprocessing.processing.filters.ExampleFilter;
import com.craftincode.imageprocessing.processing.filters.InvertFilter;
import com.craftincode.imageprocessing.processing.filters.PixelizeFilter;
import javafx.scene.image.WritableImage;

public class FiltersFactory {
    public static FilterTask createFilter(FilterType filterType, WritableImage fromImage, WritableImage toImage, int startX, int endX) {
        switch (filterType) {
            case INVERT:
                return new InvertFilter(fromImage, toImage, startX, endX);
            case BLACK_WHITE:
                return new BlackWhiteFilter(fromImage, toImage, startX, endX);
            case PIXELIZE:
                return new PixelizeFilter(fromImage, toImage, startX, endX, 200);
            case EXAMPLE:
                return new ExampleFilter(fromImage, toImage, startX, endX);
            default:
                throw new IllegalArgumentException("Incorrect filter type!");
        }
    }
}
