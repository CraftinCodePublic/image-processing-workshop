package com.craftincode.imageprocessing.processing;

import javafx.scene.image.WritableImage;

public class FilteringResult {
    private WritableImage image;
    private String filterName;
    private int threadsNumber;
    private double durationInSeconds;

    public FilteringResult(WritableImage image, String filterName, int threadsNumber, double durationInSeconds) {
        this.image = image;
        this.filterName = filterName;
        this.threadsNumber = threadsNumber;
        this.durationInSeconds = durationInSeconds;
    }

    public WritableImage getImage() {
        return image;
    }

    public String getFilterName() {
        return filterName;
    }

    public int getThreadsNumber() {
        return threadsNumber;
    }

    public double getDurationInSeconds() {
        return durationInSeconds;
    }
}
