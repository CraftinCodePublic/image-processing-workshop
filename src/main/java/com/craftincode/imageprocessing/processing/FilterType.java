package com.craftincode.imageprocessing.processing;

public enum FilterType {
    INVERT("Invert color"),
    BLACK_WHITE("Black-white"),
    PIXELIZE("Pixelize"),
    EXAMPLE("Example filter");

    private String displayName;

    FilterType(String displayName) {
        this.displayName = displayName;
    }

    @Override
    public String toString() {
        return displayName;
    }

    public String getDisplayName() {
        return displayName;
    }
}
