package com.craftincode.imageprocessing.processing;

import javafx.concurrent.Task;
import javafx.scene.image.WritableImage;

import java.util.ArrayList;
import java.util.List;

public class FilterTasksRunner extends Task<FilteringResult> {
    private final WritableImage imageFrom;
    private final FilterType filterType;
    private int threadsNumber;
    private int times;

    public FilterTasksRunner(int threadsNumber, WritableImage imageFrom, FilterType filterType, int times) {
        this.threadsNumber = threadsNumber;
        this.imageFrom = imageFrom;
        this.filterType = filterType;
        this.times = times;
    }

    private void process(WritableImage newImage) {
        List<FilterTask> taskList = new ArrayList<>();
        int threadXRange = (int) (imageFrom.getWidth() / threadsNumber);
        for (int i = 0; i < threadsNumber; i++) {
            int xStart = i * threadXRange;
            int xEnd = (i + 1) * threadXRange;
            if (i == threadsNumber - 1) {
                xEnd = (int) (imageFrom.getWidth() - 1);
            }

            taskList.add(FiltersFactory.createFilter(filterType, imageFrom, newImage, xStart, xEnd));
        }

        List<Thread> threads = new ArrayList<>();
        taskList.forEach(task -> threads.add(new Thread(task)));

        threads.forEach(Thread::start);
        threads.forEach(t -> {
            try {
                t.join();
            } catch (InterruptedException e) {
                e.printStackTrace();
                // do nothin'
            }
        });
    }

    @Override
    protected FilteringResult call() {
        WritableImage newImage = new WritableImage((int) imageFrom.getWidth(), (int) imageFrom.getHeight());

        long startTime = System.nanoTime();

        for (int i = 0; i < times; i++) {
            updateProgress(i, times);

            process(newImage);
        }

        updateProgress(1, 1);

        long duration = System.nanoTime() - startTime;
        double durationInSeconds = duration / 1000000000d;

        return new FilteringResult(newImage, filterType.getDisplayName(), threadsNumber, durationInSeconds);
    }
}
