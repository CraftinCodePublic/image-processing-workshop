package com.craftincode.imageprocessing.processing;

import javafx.concurrent.Task;
import javafx.scene.image.PixelReader;
import javafx.scene.image.PixelWriter;
import javafx.scene.image.WritableImage;

public abstract class FilterTask extends Task<WritableImage> {
    protected final int startX;
    protected final int endX;
    protected final int width;
    protected final int height;
    protected PixelReader pixelReader;
    protected PixelWriter pixelWriter;
    private WritableImage toImage;

    public FilterTask(WritableImage fromImage, WritableImage toImage, int startX, int endX) {
        this.toImage = toImage;
        this.startX = startX;
        this.endX = endX;
        this.pixelReader = fromImage.getPixelReader();
        this.pixelWriter = toImage.getPixelWriter();
        this.width = (int) fromImage.getWidth();
        this.height = (int) fromImage.getHeight();
    }

    @Override
    protected WritableImage call() throws Exception {
        process();
        return toImage;
    }

    protected abstract void process();
}
