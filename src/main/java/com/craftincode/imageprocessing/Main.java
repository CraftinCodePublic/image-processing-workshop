package com.craftincode.imageprocessing;

import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.stage.Stage;

import java.net.URL;

public class Main extends Application {

    public static void main(String[] args) {
        launch(args);
    }

    @Override
    public void start(Stage primaryStage) throws Exception {
        URL layoutResource = getClass().getClassLoader().getResource("layout.fxml");
        Parent root = FXMLLoader.load(layoutResource);
        primaryStage.setTitle("[Craftin' Code] Wielowątkowe przetwarzanie obrazów");
        primaryStage.setScene(new Scene(root));
        primaryStage.show();
    }
}
