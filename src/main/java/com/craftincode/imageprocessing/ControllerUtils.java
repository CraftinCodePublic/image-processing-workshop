package com.craftincode.imageprocessing;

import javafx.embed.swing.SwingFXUtils;
import javafx.scene.control.Alert;
import javafx.scene.image.*;

import javax.imageio.ImageIO;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;

public class ControllerUtils {
    private BufferedImage convertBufferedImageToRgb(BufferedImage im) {
        BufferedImage bufferedImage1 = new BufferedImage(im.getWidth(), im.getHeight(), BufferedImage.TYPE_INT_RGB);
        for (int x = 0; x < im.getWidth(); x++) {
            for (int y = 0; y < im.getHeight(); y++) {
                bufferedImage1.getRaster().setPixel(x, y, im.getRaster().getPixel(x, y, (int[]) null));
            }
        }
        bufferedImage1.getRaster();
        return bufferedImage1;
    }

    public void saveImageToFile(File file, ImageView imageView) {
        try {
            BufferedImage bufferedImage = SwingFXUtils.fromFXImage(imageView.getImage(), null);
            BufferedImage rgbBufferedImage = convertBufferedImageToRgb(bufferedImage);
            ImageIO.write(rgbBufferedImage, "jpg", file);
        } catch (IOException ex) {
            new Alert(Alert.AlertType.ERROR, "Error occured while saving to file");
            System.out.println(ex.getMessage());
        }
    }

    public WritableImage convertImageToWritableImage(Image image) {
        WritableImage writableImage = new WritableImage((int) image.getWidth() + 1, (int) image.getHeight() + 1);
        PixelReader pixelReader = image.getPixelReader();
        PixelWriter pixelWriter = writableImage.getPixelWriter();

        for (int y = 0; y < image.getHeight(); y++) {
            for (int x = 0; x < image.getWidth(); x++) {
                pixelWriter.setColor(x, y, pixelReader.getColor(x, y));
            }
        }
        return writableImage;
    }
}
